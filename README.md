**Fonts Site Script Free**

Setup and manage your own image sharing service with Fonts Site Script Free.

**About Fonts Site Script Free**

This fonts site script is a free original version of the premium php fonts site script available at https://mfscripts.com/font-site-script/overview.html. It allows you to create a fonts site directory.

This is a community based release that is supplied without support or liability. You are free to use the fonts site script as you wish. The only restriction to this PHP script is that this README file must not be edited or deleted. 


** Features:**

* Over 9,300 free fonts. These originated from another free fonts site on the web.
* Written in php/mysql with access to 100% of the source code.
* All pages are created using Apaches mod-rewrite so it looks like over 9,300 flat html pages to the search engines.
* Automatic creation of the font preview images.
* Custom preview images so users can view the font in their own text online.
* Character map for all fonts.
* Contents listing/view link for all font zip files. i.e. any additional readme/txt files in the distribution.
* Basic admin area to upload new fonts. From this the previews, contents listing etc are all automatically created.
* User rating for each font.
* Total downloads for each font.
* Overall downloads by day/all time displayed in the top right of the pages.
* Browse page organised by letter.
* Search page.
* Admin maintenance - clear font preview cache.
* Control preview font size via config.
* Control preview text via config. Use the font name or your own text.

**Requirements**

* php 4.x/5.x
* mysql
* mod rewrite module on apache
* gd library with freetype
* At least 4GB of space. (this is mainly for the fonts. If you upload less fonts on the site then obviously less space is required.)

**Installation**

Full instructions are given in the ___SETUP_INSTRUCTIONS.txt file found within the repository.

**Contributions**

If you'd like to contribute to the project, please contact us via the project https://bitbucket.org/MFScripts/fonts-site-script-free

**License**

Fonts Site Script Free is copyrighted by http://mfscripts.com and is released under the http://opensource.org/licenses/MIT. You are free to use the fonts site script as you wish. The only restriction to this PHP script is that this README file must not be edited or deleted. 

**Support**

This code is released without any support and as-is. Any support requests on this version will be removed. For community driven support please use the project https://bitbucket.org/MFScripts/fonts-site-script-free